package elementos;

import java.util.ArrayList;
import java.util.Random;

public class Baralho {
	protected ArrayList<Carta> cartas = new ArrayList<>();
	private Random random = new Random();
	
	public void inicializar() {
		cartas = new ArrayList<>();
		
		for(Rank rank: Rank.values()) {
			for(Naipe naipe: Naipe.values()) {
				Carta carta = new Carta(rank, naipe);
				
				cartas.add(carta);
			}
		}
	}
	
	public Carta pegarTopo() {
		return cartas.remove(0);
	}
	
	public boolean vazio() {
		return cartas.size() == 0;
	}
	
	public Carta pegarAleatoria() {
		int sorteio = random.nextInt(cartas.size());
		
		return cartas.remove(sorteio);
	}
}
