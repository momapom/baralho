package truco;

import java.util.ArrayList;

import elementos.Baralho;
import elementos.Carta;
import elementos.Naipe;
import elementos.Rank;

public class BaralhoTruco extends Baralho{
	@Override
	public void inicializar() {
		cartas = new ArrayList<>();
		for(Rank rank: Rank.values()) {
			for(Naipe naipe: Naipe.values()) {
				Carta carta = new Carta(rank, naipe);

				if(PontuacaoTruco.getValor(rank) != -1) {
					cartas.add(carta);
				}
			}
		}
	}
}

