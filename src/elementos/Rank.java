package elementos;

public enum Rank {
	As("Ás"),
	Dois("Dois"),
	Tres("Três"),
	Quatro("Quatro"),
	Cinco("Cinco"),
	Seis("seis"),
	Sete("Sete"),
	Oito("Oito"),
	Nove("Nove"),
	Dez("Dez"),
	Valete("Valete"),
	Dama("Dama"),
	Rei("Rei");
	
	private final String nome;
	
	Rank(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return this.nome;
	}
}
