package main;

import elementos.Baralho;
import elementos.Carta;
import truco.BaralhoTruco;
import truco.PontuacaoTruco;
import vinteum.PontuacaoVinteUm;

public class App {
	public static void main(String[] args) {
		Baralho baralho = new BaralhoTruco();
		baralho.inicializar();
		
		Carta carta = baralho.pegarAleatoria();
		
		System.out.println(carta);
		
		int valor = PontuacaoTruco.getValor(carta.getRank());
		
		System.out.println(valor);
	}
}
